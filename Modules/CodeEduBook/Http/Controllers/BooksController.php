<?php

namespace CodeEduBook\Http\Controllers;

use CodeEduBook\Http\Requests\BookCreateRequest;
use CodeEduBook\Repositories\BookRepository;
use CodeEduBook\Http\Requests\BookUpdateRequest;
use CodeEduBook\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use ControlUser\Annotations\Mapping as Permission;

/**
 * Class BooksController
 * @package CodeEduBook\Http\Controllers
 * @Permission\Controller(name="book-admin", description="Administração de livros")
 */
class BooksController extends Controller
{
    /**
     * @var BookRepository
     */
    protected $repository;
    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * BooksController constructor.
     * @param BookRepository $repository
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(BookRepository $repository, CategoryRepository $categoryRepository)
    {
        $this->repository = $repository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @Permission\Action(name="list", description="Ver listagem de livros")
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
//        $this->repository->pushCriteria(new FindByTitleCriteria($search));
        $books = $this->repository->paginate();
        //dd($books);
        return view('codeedubook::books.index', compact('books', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @Permission\Action(name="store", description="Criar livro")
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->categoryRepository->lists('name', 'id'); //pluck
        return view('codeedubook::books.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @Permission\Action(name="store", description="Criar livro")
     * @param  \CodeEduBook\Http\Requests\BookCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookCreateRequest $request)
    {
        $input = $request->all();
        $input['author_id'] = \Auth::user()->id;
        $this->repository->create($input);
        $url = $request->get('redirect_to', route('books.index'));
        $request->session()->flash('message', 'Livro cadastrado com sucesso.');
        return redirect()->to($url);
    }

    /**
     * Display the specified resource.
     *
     * @Permission\Action(name="update", description="Atualizar livro")
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @Permission\Action(name="update", description="Atualizar livro")
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = $this->repository->find($id);
        $this->categoryRepository->withTrashed();
        $categories = $this->categoryRepository->listsWithMutators('name_trashed', 'id'); //pluck
        return view('codeedubook::books.edit', compact('book', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @Permission\Action(name="update", description="Atualizar livro")
     * @param  BookUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookUpdateRequest $request, $id)
    {
        $input = $request->except(['author_id']);
        $this->repository->update($input, $id);
        $url = $request->get('redirect_to', route('books.index'));
        $request->session()->flash('message', 'Livro alterado com sucesso.');
        return redirect()->to($url);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @Permission\Action(name="destroy", description="Excluir livro")
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param  Book  $book
     * @internal param int $id
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        \Session::flash('message', 'Livro excluido com sucesso.');
        return redirect()->to(\URL::previous());
    }
}
