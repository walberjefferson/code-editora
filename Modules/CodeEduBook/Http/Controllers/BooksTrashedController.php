<?php

namespace CodeEduBook\Http\Controllers;

use CodeEduBook\Repositories\BookRepository;
use Illuminate\Http\Request;
use ControlUser\Annotations\Mapping as Permission;

/**
 * Class BooksTrashedController
 * @package CodeEduBook\Http\Controllers
 * @Permission\Controller(name="book-trashed-admin", description="Administração da lixeira de livros")
 */
class BooksTrashedController extends Controller
{
    /**
     * @var BookRepository
     */
    protected $repository;
    /**
     * @var
     */
    protected $categoryRepository;

    /**
     * BooksTrashedController constructor.
     * @param BookRepository $repository
     */
    public function __construct(BookRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @Permission\Action(name="list", description="Ver lixeira")
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $books = $this->repository->onlyTrashed()->paginate();
        return view('codeedubook::trashed.books.index', compact('books', 'search'));
    }

    /**
     * @param $id
     * @Permission\Action(name="show", description="Ver livro da lixeira")
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $book = $this->repository->onlyTrashed()->find($id);
        return view('codeedubook::trashed.books.show', compact('book'));
    }

    /**
     * @Permission\Action(name="update", description="Restaurar livro da lixeira")
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->repository->onlyTrashed()->restore($id);
        $url = $request->get('redirect_to', route('trashed.books.index'));
        $request->session()->flash('message', 'Livro restaurado com sucesso.');
        return redirect()->to($url);
    }
}
