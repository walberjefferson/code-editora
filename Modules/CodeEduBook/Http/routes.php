<?php

Route::group(['middleware' => ['auth', 'isVerified', 'auth.resource']], function () {
    //Rotas de Categorias
    Route::resource('categories', 'CategoriesController', ['except' => 'show']);
    Route::resource('books', 'BooksController', ['except' => 'show']);
    Route::group(['prefix' => 'trashed', 'as' => 'trashed.'], function () {
        Route::resource('books', 'BooksTrashedController', ['only' => ['index', 'show', 'update']]);
    });
});