<?php

namespace CodeEduBook\Models;

use ControlUser\Models\User;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Book extends Model implements Transformable
{
    use TransformableTrait, FormAccessible, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'title',
        'subtitle',
        'price',
        'author_id'
    ];

    public function author()
    {
        return $this->belongsTo(User::class); // Muitos para um
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class)->withTrashed(); //Muitos para Muitos
    }

    public function formCategoriesAttribute()
    {
        return $this->categories()->pluck('id')->all();
    }

}
