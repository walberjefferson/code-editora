@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Livros da lixeira: {{ $book->title }}</h3>
                <ul>
                    <li class="list-group-item"><strong>Titulo</strong></li>
                    <li class="list-group-item">{{ $book->title }}</li>
                    <li class="list-group-item"><strong>Subtitulo</strong></li>
                    <li class="list-group-item">{{$book->subtitle}}</li>
                    <li class="list-group-item"><strong>Preço</strong></li>
                    <li class="list-group-item">{{$book->price}}</li>
                    <li class="list-group-item"><strong>Categorias</strong></li>
                    <li class="list-group-item">{{$book->categories->implode('name_trashed', ', ')}}</li>
                </ul>
            </div>
        </div>
    </div>

@endsection