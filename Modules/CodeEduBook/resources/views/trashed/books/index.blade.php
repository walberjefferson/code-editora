@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Lixeira de livros</h1>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                {!! Form::model(compact('search'), ['class' => 'form', 'method' => 'GET']) !!}
                <div class="input-group">
                    {!! Form::text('search', null, ['class' => 'form-control', 'placeholder' => 'Pesquisa por título']) !!}
                    <div class="input-group-btn">
                        {!! Button::primary('Buscar')->submit() !!}
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                @if($books->count() > 0)
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th width="4%" class="text-center">ID</th>
                            <th>Titulo</th>
                            <th>Sub-titulo</th>
                            <th>Autor</th>
                            <th>Preço</th>
                            <th width="6%">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($books as $book)
                            <tr>
                                <td class="text-center">{{ $book->id }}</td>
                                <td>{{ $book->title }}</td>
                                <td>{{ $book->subtitle }}</td>
                                <td>{{ $book->author->name }}</td>
                                <td>{{ $book->price }}</td>
                                <td class="text-center">
                                    <div class="btn-group btn-group-xs">
                                        <a href="{{ route('trashed.books.show', $book->id) }}"
                                           class="btn btn-link">Ver</a>
                                        <?php $deleteForm = "delete-form-{$loop->index}" ?>
                                        <a href="{{ route('trashed.books.update', $book->id) }}" class="btn btn-link"
                                           onclick="event.preventDefault(); document.getElementById('{{$deleteForm}}').submit();">Restaurar</a>
                                        {!! Form::open(['route' => ['trashed.books.update', $book->id], 'id' => $deleteForm, 'style' => 'display:none;', 'method' => 'PUT']) !!}
                                        {!! Form::hidden('redirect_to', URL::previous()) !!}
                                        {!! Form::close() !!}
                                    </div>


                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <span class="text-center center-block">{!! $books->render() !!}</span>
                @else
                    <div class="well">
                        <p class="text-center"><strong>Nenhum livro na lixeira</strong></p>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection