@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h3>Nova categoria</h3>
            {!! Form::open(['route' => 'books.store', 'class' => 'form']) !!}
                @include('codeedubook::books._form')
                <div class="form-group">
                    {!! Form::submit('Cadastrar Livro', ['class' => 'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection