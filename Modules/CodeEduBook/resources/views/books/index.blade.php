@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Listagem de livros
                    @can('book-admin/store')
                        {!! Button::link('<i class="glyphicon glyphicon-plus"></i> Novo Livro')->asLinkTo(route('books.create')) !!}
                    @endcan
                </h1>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                {!! Form::model(compact('search'), ['class' => 'form', 'method' => 'GET']) !!}
                <div class="input-group">
                    {!! Form::text('search', null, ['class' => 'form-control', 'placeholder' => 'Pesquisa por título']) !!}
                    <div class="input-group-btn">
                        {!! Button::primary('Buscar')->submit() !!}
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th width="4%" class="text-center">ID</th>
                        <th>Titulo</th>
                        <th>Sub-titulo</th>
                        <th>Autor</th>
                        <th>Preço</th>
                        @can('book-admin/update')
                            <th width="6%">Ações</th>
                        @endcan
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($books as $book)
                        <tr>
                            <td class="text-center">{{ $book->id }}</td>
                            <td>{{ $book->title }}</td>
                            <td>{{ $book->subtitle }}</td>
                            <td>{{ $book->author->name }}</td>
                            <td>{{ $book->price }}</td>
                            @can('book-admin/update')
                                <td class="text-center">
                                    <div class="btn-group btn-group-xs">
                                        <a href="{{ route('books.edit', $book->id) }}" class="btn btn-link"><i
                                                    class="glyphicon glyphicon-pencil"></i></a>
                                        <?php $deleteForm = "delete-form-{$loop->index}" ?>
                                        <a href="{{ route('books.destroy', $book->id) }}" class="btn btn-link"
                                           onclick="event.preventDefault(); document.getElementById('{{$deleteForm}}').submit();"><i
                                                    class="glyphicon glyphicon-trash"></i></a>
                                        {!! Form::open(['route' => ['books.destroy', $book->id], 'id' => $deleteForm, 'style' => 'display:none;', 'method' => 'DELETE']) !!}
                                        {!! Form::close() !!}
                                    </div>
                                </td>
                            @endcan
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <span class="text-center center-block">{!! $books->render() !!}</span>
            </div>
        </div>
    </div>

@endsection