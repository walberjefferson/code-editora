@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h3>Editando livro</h3>

            {!! Form::model($book, ['method' => 'PUT','route' => ['books.update', $book->id], 'class' => 'form']) !!}
                @include('codeedubook::books._form')
                <div class="form-group">
                    {!! Form::submit('Salvar Livro', ['class' => 'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection