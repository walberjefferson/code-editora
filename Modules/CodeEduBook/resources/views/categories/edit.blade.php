@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h3>Editando categoria</h3>

            {!! Form::model($category, ['method' => 'PUT','route' => ['categories.update', $category->id], 'class' => 'form']) !!}
            @include('codeedubook::categories._form')
            <div class="form-group">
                {!! Button::primary('Salvar Categoria')->submit() !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection