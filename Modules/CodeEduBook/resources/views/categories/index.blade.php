@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Listagem de categorias
                    @can('categories-admin/store')
                        {!! Button::link('<i class="glyphicon glyphicon-plus"></i> Nova Categoria')->asLinkTo(route('categories.create')) !!}
                    @endcan
                </h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th width="4%" class="text-center">ID</th>
                        <th>Nome</th>
                        @can('categories-admin/update')
                            <th width="6%">Ações</th>
                        @endcan
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td class="text-center">{{ $category->id }}</td>
                            <td>{{ $category->name }}</td>
                            @can('categories-admin/update')
                                <td class="text-center">
                                    <div class="btn-group btn-group-xs">
                                        <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-link">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </a>
                                        <?php $deleteForm = "delete-form-{$loop->index}" ?>
                                        <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-link"
                                           onclick="event.preventDefault(); document.getElementById('{{$deleteForm}}').submit();"><i
                                                    class="glyphicon glyphicon-trash"></i></a>
                                        {!! Form::open(['route' => ['categories.destroy', $category->id], 'id' => $deleteForm, 'style' => 'display:none;', 'method' => 'DELETE']) !!}
                                        {!! Form::close() !!}
                                    </div>
                                </td>
                            @endcan
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <span class="text-center center-block">{!! $categories->render() !!}</span>
            </div>
        </div>
    </div>

@endsection