<?php

namespace ControlUser\Models;

use CodeEduBook\Models\Book;
use Collective\Html\Eloquent\FormAccessible;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class User extends Authenticatable implements Transformable
{

    use Notifiable, TransformableTrait, SoftDeletes, FormAccessible;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function generatePassword($password = null)
    {
        return !$password ? bcrypt(str_random(8)) : bcrypt($password);
    }

    public function books()
    {
        return $this->hasMany(Book::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function formRolesAttribute()
    {
        return $this->roles->pluck('id')->all();
    }

    /**
     * @param Collection|string $role
     * @return boolean
     */
    public function hasRole($role)
    {
        return is_string($role) ?
            $this->roles->contains('name', $role) :
            (boolean) $role->intersect($this->roles)->count();
    }

    public function isAdmin()
    {
        return $this->hasRole(config('controluser.acl.role_admin'));
    }
}
