<?php

namespace ControlUser\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoleRepository
 * @package namespace CodePub\Repositories;
 */
interface RoleRepository extends RepositoryInterface
{
    //
}
