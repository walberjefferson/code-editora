<?php

namespace ControlUser\Repositories;

use Prettus\Repository\Contracts\RepositoryCriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PermissionRepository
 * @package namespace CodePub\Repositories;
 */
interface PermissionRepository extends RepositoryInterface, RepositoryCriteriaInterface
{
    //
}
