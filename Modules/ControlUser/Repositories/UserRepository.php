<?php

namespace ControlUser\Repositories;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepositoryRepository
 * @package namespace CodePub\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    //
}
