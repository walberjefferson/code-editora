<?php

namespace ControlUser\Facade;

use ControlUser\Annotations\PermissionReader as PermissionReaderService;
use Illuminate\Support\Facades\Facade;

class PermissionReader extends Facade
{
    protected static function getFacadeAccessor()
    {
        return PermissionReaderService::class;
    }
}