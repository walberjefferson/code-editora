<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(\ControlUser\Models\User::class, 1)->create();
        //\ControlUser\Models\User::truncate();
        
        factory(\ControlUser\Models\User::class)->create(['name' => 'Admin', 'email' => 'admin@admin.com', 'password' => bcrypt(123456)]);
        factory(\ControlUser\Models\User::class)->create(['name' => 'Editor', 'email' => 'editora@admin.com', 'password' => bcrypt(123456)]);
    }
}
