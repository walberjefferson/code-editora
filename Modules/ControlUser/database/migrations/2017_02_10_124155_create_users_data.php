<?php

use ControlUser\Models\User;
use Illuminate\Database\Migrations\Migration;

class CreateUsersData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Database\Eloquent\Model::unguard();
        User::create([
            'name' => config('controluser.user_default.name'),
            'email' => config('controluser.user_default.email'),
            'password' => bcrypt(config('controluser.user_default.password')),
            'remember_token' => str_random(10),
            'verified' => true
        ]);
        \Illuminate\Database\Eloquent\Model::reguard();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::disableForeignKeyConstraints();
        $user = User::where('email', config('controluser.user_default.email'))->first();
        $user->forceDelete();
        \Schema::enableForeignKeyConstraints();
    }
}
