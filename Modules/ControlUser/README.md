# Controle de Usuários

##### _Modulo para criação de usuários_

## Modo de utilização

##### 1 - Registrar no composer.json
````
"ControlUser\\": "Modules/ControlUser/"`
````
##### 2 - Registrar o Service Provider
````
ControlUser\Providers\ControlUserServiceProvider::class,
````
##### 3 - Dara o comando `compose dump`
##### 4 - Publicar as migrations `php artisan vendor:publish --provider="ControlUser\Providers\ControlUserServiceProvider" --tag=migrations --force`
##### 5 - Publicar as seeders `php artisan vendor:publish --provider="ControlUser\Providers\ControlUserServiceProvider" --tag=seeders --force`
##### 6 - Publicar as configurações `php artisan vendor:publish --provider="ControlUser\Providers\ControlUserServiceProvider" --tag=config --force`
##### 7 - Publicar as views (se necessário) `php artisan vendor:publish --provider="ControlUser\Providers\ControlUserServiceProvider" --tag=views --force`

## Pacotes necessários

````
nwidart/laravel-modules
jrean/laravel-user-verification
doctrine/annotations
prettus/l5-repository
````
