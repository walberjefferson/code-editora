@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h3>Editar o perfil de: {{$user->name}}</h3>
            {!! Form::open(['route' => 'controluser.user_settings.update', 'class' => 'form', 'method' => 'PUT']) !!}
            @include('controluser::user-settings._form')
            <div class="form-group">
                {!! Button::primary('Salvar')->submit() !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection