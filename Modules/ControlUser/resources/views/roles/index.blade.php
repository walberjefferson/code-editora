@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Listagem de usuários
                    {!! Button::link('<i class="glyphicon glyphicon-plus"></i> Novo papel de usuário')->asLinkTo(route('controluser.roles.create')) !!}
                </h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th width="4%" class="text-center">ID</th>
                        <th>Nome</th>
                        <th>Descrição</th>
                        <th width="8%">Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($roles as $role)
                        <tr>
                            <td class="text-center">{{ $role->id }}</td>
                            <td>{{ $role->name }}</td>
                            <td>{{ $role->description }}</td>
                            <td class="text-center">
                                <div class="btn-group btn-group-xs">
                                    <a href="{{ route('controluser.roles.edit', $role->id) }}" class="btn btn-link"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <?php $deleteForm = "delete-form-{$loop->index}" ?>
                                    <a href="{{ route('controluser.roles.edit', $role->id) }}" class="btn btn-link"
                                       onclick="event.preventDefault(); document.getElementById('{{$deleteForm}}').submit();"><i class="glyphicon glyphicon-trash"></i></a>
                                    {!! Form::open(['route' => ['controluser.roles.destroy', $role->id], 'id' => $deleteForm, 'style' => 'display:none;', 'method' => 'DELETE']) !!}
                                    {!! Form::close() !!}
                                    <a href="{{ route('controluser.roles.permissions.edit', $role->id) }}" class="btn btn-link"><i class="glyphicon glyphicon-cog"></i></a>
                                </div>


                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <span class="text-center center-block">{!! $roles->render() !!}</span>
            </div>
        </div>
    </div>

@endsection