@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h3>Permissões de {{ $role->name }}</h3>

            {!! Form::open(['method' => 'PUT','route' => ['controluser.roles.permissions.update', $role->id], 'class' => 'form']) !!}

            <ul class="list-group">
                @foreach($permissionsGroup as $pg)
                    <li class="list-group-item">
                        <h4 class="list-group-item-heading">{{ $pg->description }}</h4>
                        <p class="list-group-item-text">
                        <ul class="list-inline">
                            @php
                                $permissionsSubGroup = $permissions->filter(function ($value) use ($pg){
                                    return $value->name == $pg->name;
                                });
                            @endphp
                            @foreach($permissionsSubGroup as $permission)
                                <li>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="permissions[]"
                                                   {{ $role->permissions->contains('id', $permission->id) ? 'checked="checked"' : '' }} value="{{$permission->id}}">
                                            {{ $permission->resource_description }}
                                        </label>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        </p>
                    </li>
                @endforeach
            </ul>

            <div class="form-group">
                {!! Button::primary('Salvar')->submit() !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection