@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Listagem de usuários
                    {!! Button::link('<i class="glyphicon glyphicon-plus"></i> Novo Usuário')->asLinkTo(route('controluser.users.create')) !!}
                </h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th width="4%" class="text-center">ID</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Roles</th>
                        <th width="6%">Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td class="text-center">{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->roles->implode('name', ' | ') }}</td>
                            <td class="text-center">
                                <div class="btn-group btn-group-xs">
                                    <a href="{{ route('controluser.users.edit', $user->id) }}" class="btn btn-link"><i
                                                class="glyphicon glyphicon-pencil"></i></a>

                                    @if($user->id == \Auth::user()->id)
                                        <a href="#" class="btn btn-link text-danger disabled">
                                            <i class="glyphicon glyphicon-trash"></i>
                                        </a>
                                    @else
                                        <?php $deleteForm = "delete-form-{$loop->index}" ?>
                                        <a href="{{ route('controluser.users.edit', $user->id) }}" class="btn btn-link"
                                           onclick="event.preventDefault(); document.getElementById('{{$deleteForm}}').submit();">
                                            <i class="glyphicon glyphicon-trash"></i>
                                        </a>
                                        {!! Form::open(['route' => ['controluser.users.destroy', $user->id], 'id' => $deleteForm, 'style' => 'display:none;', 'method' => 'DELETE']) !!}
                                        {!! Form::close() !!}
                                    @endif
                                </div>


                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <span class="text-center center-block">{!! $users->render() !!}</span>
            </div>
        </div>
    </div>

@endsection