@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h3>Novo usuário</h3>
            {!! Form::open(['route' => 'controluser.users.store', 'class' => 'form']) !!}
                @include('controluser::users._form')
                <div class="form-group">
                    {!! Button::primary('Criar Usuário')->submit() !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection