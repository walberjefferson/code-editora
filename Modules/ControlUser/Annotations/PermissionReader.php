<?php

namespace ControlUser\Annotations;


use ControlUser\Annotations\Mapping\Action;
use ControlUser\Annotations\Mapping\Controller;
use Doctrine\Common\Annotations\Reader;

/**
 * Class PermissionReader
 * @package ControlUser\Annotations
 */
class PermissionReader
{

    /**
     * @var Reader
     */
    private $reader;

    /**
     * PermissionReader constructor.
     * @var Reader $reader
     */
    public function __construct(Reader $reader)
    {
        $this->reader = $reader;
    }

    /**
     * @return array
     */
    public function getPermissions()
    {
        $controllersClasses = $this->getControllers();
        $declarated = get_declared_classes();
        $permissions = [];
        foreach ($declarated as $className) {
            $rc = new \ReflectionClass($className);
            if (in_array($rc->getFileName(), $controllersClasses)) {
                $permission = $this->getPermission($className);
                if (count($permission)) {
                    $permissions = array_merge($permissions, $permission);
                }
            }
        }
        return $permissions;
    }

    /**
     * @param $controllerClass
     * @param null $action
     * @return array
     */
    public function getPermission($controllerClass, $action = null)
    {
        $rc = new \ReflectionClass($controllerClass);
        /** @var Controller $controllerAnnotation */
        $controllerAnnotation = $this->reader->getClassAnnotation($rc, Controller::class);
        $permissions = [];
        if ($controllerAnnotation) {
            $permission = [
                'name' => $controllerAnnotation->name,
                'description' => $controllerAnnotation->description
            ];
        }
        $rcMethods = !$action ? $rc->getMethods() : [$rc->getMethod($action)];
        foreach ($rcMethods as $rcMethod) {
            /** @var Action $actionAnnotation */
            $actionAnnotation = $this->reader->getMethodAnnotation($rcMethod, Action::class);
            if ($actionAnnotation) {
                $permission['resource_name'] = $actionAnnotation->name;
                $permission['resource_description'] = $actionAnnotation->description;
                $permissions[] = (new \ArrayIterator($permission))->getArrayCopy();
            }
        }
        return $permissions;
    }

    /**
     * @return array
     */
    private function getControllers()
    {
        $dirs = config('controluser.acl.controllers_annotations');
        $files = [];
        foreach ($dirs as $dir) {
            foreach (\File::allFiles($dir) as $file) {
                $files[] = $file->getRealPath();
                require_once $file->getRealPath();
            }
        }
        return $files;
    }
}