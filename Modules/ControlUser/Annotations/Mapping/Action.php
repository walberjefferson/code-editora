<?php

namespace ControlUser\Annotations\Mapping;


/**
 * Class Action
 * @package ControlUser\Annotations\Mapping
 * @Annotation
 * @Target("METHOD")
 */
class Action
{
    public $name;
    public $description;
}