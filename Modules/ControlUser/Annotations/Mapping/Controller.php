<?php

namespace ControlUser\Annotations\Mapping;

/**
 * Class Controller
 * @package ControlUser\Annotations\Mapping
 * @Annotation
 * @Target("CLASS")
 */
class Controller
{
    public $name;
    public $description;
}
