<?php

namespace ControlUser\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\ControlUser\Repositories\UserRepository::class, \ControlUser\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\ControlUser\Repositories\PermissionRepository::class, \ControlUser\Repositories\PermissionRepositoryEloquent::class);
        $this->app->bind(\ControlUser\Repositories\RoleRepository::class, \ControlUser\Repositories\RoleRepositoryEloquent::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
