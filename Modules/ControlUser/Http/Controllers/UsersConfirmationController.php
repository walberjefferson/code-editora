<?php

namespace ControlUser\Http\Controllers;

use ControlUser\Repositories\UserRepository;
use Jrean\UserVerification\Traits\VerifiesUsers;

class UsersConfirmationController extends Controller
{
    use VerifiesUsers;

    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function redirectAfterVerification()
    {
        $this->loginUser();
        return route('controluser.user_settings.edit');
    }

    private function loginUser()
    {
        $email = \Request::get('email');
        $user = $this->repository->findByField('email', $email)->first();
        \Auth::login($user);
    }

}
