<?php

namespace ControlUser\Http\Controllers;

use ControlUser\Criteria\FindPermissionsGroupCriteria;
use ControlUser\Criteria\FindPermissionsResourceCriteria;
use ControlUser\Http\Requests\PermissionRequest;
use ControlUser\Http\Requests\RoleDeleteRequest;
use ControlUser\Http\Requests\RoleRequest;
use ControlUser\Repositories\PermissionRepository;
use ControlUser\Repositories\RoleRepository;
use Illuminate\Database\QueryException;
use Illuminate\Http\Response;
use ControlUser\Annotations\Mapping as Permission;

/**
 * Class RolesController
 * @package ControlUser\Http\Controllers
 * @Permission\Controller(name="roles-admin", description="Administração de papeis de usuários")
 */
class RolesController extends Controller
{
    /**
     * @var RoleRepository
     */
    private $repository;
    /**
     * @var PermissionRepository
     */
    private $permissionRepository;

    /**
     * RolesController constructor.
     */
    public function __construct(RoleRepository $repository, PermissionRepository $permissionRepository)
    {

        $this->repository = $repository;
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @Permission\Action(name="list", description="Listar papeis de usuários")
     * @return Response
     */
    public function index()
    {
        $roles = $this->repository->paginate(10);
        return view('controluser::roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @Permission\Action(name="store", description="Criar papeis de usuários")
     * @return Response
     */
    public function create()
    {
        return view('controluser::roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @Permission\Action(name="store", description="Criar papeis de usuários")
     * @param  RoleRequest $request
     * @return Response
     */
    public function store(RoleRequest $request)
    {
        $this->repository->create($request->all());
        $url = $request->get('redirect_to', route('controluser.roles.index'));
        $request->session()->flash('message', 'Papel de usuário cadastrado com sucesso');
        return redirect()->to($url);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @Permission\Action(name="update", description="Atualizar papel de usuários")
     * @return Response
     */
    public function edit($id)
    {
        $role = $this->repository->find($id);
        return view('controluser::roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @Permission\Action(name="update", description="Atualizar papel de usuários")
     * @param  RoleRequest $request
     * @return Response
     */
    public function update(RoleRequest $request, $id)
    {
        $inputs = $request->except('permissions');
        $this->repository->update($inputs, $id);
        $url = $request->get('redirect_to', route('controluser.roles.index'));
        $request->session()->flash('message', 'Papel de usuário alterado com sucesso');
        return redirect()->to($url);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @Permission\Action(name="destroy", description="Excluir papel de usuários")
     * @return Response
     */
    public function destroy(RoleDeleteRequest $request, $id)
    {
        try{
            $this->repository->delete($id);
            \Session::flash('message', 'Papel de usuário excluido com sucesso');
        }catch (QueryException $ex){
            \Session::flash('error', 'Papel de usuário não pode ser excluido. Ele está relacionado com outros registros.');
        }


        return redirect()->to(\URL::previous());
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editPermission($id)
    {
        $this->permissionRepository->pushCriteria(new FindPermissionsResourceCriteria());
        $permissions = $this->permissionRepository->all();
        $this->permissionRepository->resetCriteria();

        $this->permissionRepository->pushCriteria(new FindPermissionsGroupCriteria());
        $permissionsGroup = $this->permissionRepository->all(['name', 'description']);
        $role = $this->repository->find($id);

        return view('controluser::roles.permission', compact('role', 'permissions', 'permissionsGroup'));
    }

    public function updatePermission(PermissionRequest $request, $id)
    {
        $inputs = $request->only('permissions');
        $this->repository->update($inputs, $id);
        $url = $request->get('redirect_to', route('controluser.roles.index'));
        $request->session()->flash('message', 'Permissões atribuidas com sucesso');
        return redirect()->to($url);
    }
}
