<?php

namespace ControlUser\Http\Controllers;

use ControlUser\Http\Requests\UserSettingRequest;
use ControlUser\Repositories\UserRepository;


/**
 * Class UsersSettingsController
 * @package ControlUser\Http\Controllers
 */
class UsersSettingsController extends Controller
{

    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param User $user
     * @internal param int $id
     */
    public function edit()
    {
        $user = \Auth::user();
        return view('controluser::user-settings.settings', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \CodeEduBook\Http\Requests\UserSettingRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param User $user
     * @internal param int $id
     */
    public function update(UserSettingRequest $request)
    {
        $user = \Auth::user();
        $this->repository->update($request->all(), $user->id);
        $request->session()->flash('message', 'Usuário alterado com sucesso!');
        return redirect()->route('controluser.users.index');
    }

}
