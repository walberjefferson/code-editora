@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h3>Novo papel de usuário</h3>
            {!! Form::open(['route' => 'controluser.roles.store', 'class' => 'form']) !!}
                @include('controluser::roles._form')
                <div class="form-group">
                    {!! Button::primary('Criar papel de usuário')->submit() !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection