@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h3>Editando usuário</h3>

            {!! Form::model($role, ['method' => 'PUT','route' => ['controluser.roles.update', $role->id], 'class' => 'form']) !!}
            @include('controluser::roles._form')
            <div class="form-group">
                {!! Button::primary('Salvar papel de usuário')->submit() !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection