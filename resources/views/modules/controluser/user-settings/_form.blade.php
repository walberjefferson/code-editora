{!! Html::openFormGroup('password', $errors) !!}
    {!! Form::label('password', 'Senha', ['class' => 'control-label']) !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
    {!! Form::error('password', $errors) !!}
{!! Html::closeFormGroup() !!}

{!! Form::hidden('redirect_to', URL::previous()) !!}
{!! Html::openFormGroup() !!}
{!! Form::label('password_confirmation', 'Confirme sua senha', ['class' => 'control-label']) !!}
{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
{!! Html::closeFormGroup() !!}