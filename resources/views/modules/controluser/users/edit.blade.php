@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h3>Editando usuário</h3>

            {!! Form::model($user, ['method' => 'PUT','route' => ['controluser.users.update', $user->id], 'class' => 'form']) !!}
            @include('controluser::users._form')
            <div class="form-group">
                {!! Button::primary('Salvar Usuário')->submit() !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection