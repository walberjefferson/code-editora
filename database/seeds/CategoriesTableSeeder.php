<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //\CodeEduBook\Models\Category::truncate();
        
        factory(\CodeEduBook\Models\Category::class, 50)->create();
    }
}
