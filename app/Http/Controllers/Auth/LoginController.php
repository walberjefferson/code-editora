<?php

namespace CodePub\Http\Controllers\Auth;

use CodePub\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

//    public function login(Request $request)
//    {
//        $this->validate($request, [
//            'email' => 'required|email|max:255',
//            'password' => 'required|min:6',
//        ]);
//        $credentials = $request->only('email', 'password');
//        $remember = $request->has('remember');
//        if (!Auth::attempt($credentials, $remember)) {
//            return redirect()->route('login')
//                ->with(['fail' => 'Usuário ou senha inválido!'])
//                ->withInput($credentials);
//        }
//        if (!Auth::attempt(array_merge($credentials, ['verified' => 1]), $remember)) {
//            return redirect()->route('login')
//                ->with(['fail' => 'Usuário não ativo...'])
//                ->withInput($credentials);
//        }
//        return redirect()->route('categories.index');
//    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->credentials($request);


        if ($this->guard()->attempt(array_merge($credentials, ['verified' => 1]), $request->has('remember'))) {

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

}
