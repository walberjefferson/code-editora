<?php

namespace CodePub\Criteria;


trait CriteriaTrashedTrait
{
    public function onlyTrashed()
    {
        $this->pushCriteria(FindByOnlyTrashedCriteria::class);
        return $this;
    }

    public function withTrashed()
    {
        $this->pushCriteria(FindWithTrashedCriteria::class);
        return $this;
    }
}